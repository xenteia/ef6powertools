﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Design.PluralizationServices;
using System.Linq;
using System.Text;

namespace Microsoft.DbContextPackage.Pluralization
{
    public class SpanishPluralizationService : PluralizationService, ICustomPluralizationMapping
    {
        public void AddWord(string singular, string plural)
        {
            throw new NotImplementedException();
        }

        public override bool IsPlural(string word)
        {
            return !IsSingular(word);
        }

        public override bool IsSingular(string word)
        {
            var ending = reverseArray(word);

            return ending[0] != 's';
        }

        public override string Pluralize(string word)
        {
            List<string> list = new List<string>();
            var ccw = SplitCamelCase(word);

            list.Add(PluralizeWord(ccw.First()));

            foreach (var w in ccw.Skip(1))
            {
                list.Add(w);
            }

            return String.Join("", list.ToArray());
        }

        public string PluralizeWord(string word)
        {
            if(IsPlural(word))
            {
                return Normalize(word, "P", 32);
            }

            var ending = reverseArray(word);
            if (endsWithVowel(word))
            {
                if (ending[0] == 'i' || ending[0] == 'u')
                {
                    return Normalize(word + "es", "P", 40);
                }
                return Normalize(word + "s", "P", 42);
            }
            if (ending[0] != 'z')
            {
                return Normalize(word + "es", "P", 46);
            }
            else
            {
                ending[0] = 'c';
                return Normalize(word + "es", "P", 51);
            }
        }

        public override string Singularize(string word)
        {
            List<string> list = new List<string>();
            var ccw = SplitCamelCase(word);
            foreach(var w in ccw)
            {
                list.Add(SingularizeWord(w));
            }

            return String.Join("", list.ToArray());
        }

        private string SingularizeWord(string word)
        {
            if(IsSingular(word))
            {
                return Normalize(word, "S", 59);
            }

            var ending = reverseArray(word);

            if (ending[0] == 's' && ending[1] == 'e')
            {
                ending = ending.Skip(2).ToArray();

                // Singular termina en Z
                if(ending[0] == 'c' && isVowel(ending[1])) // CES
                {
                    return Normalize(new string(ending.Skip(1).Reverse().ToArray()) + "z", "S", 72);
                }

                // Termina en consonante pero la anterios es consonante tambien
                if (isConsonant(ending[0]) && isConsonant(ending[1]))
                {
                    return Normalize(new string(ending.Reverse().ToArray()) + "e", "S", 78);
                }

                if(isConsonant(ending[0]))
                {
                    return Normalize(ending.Reverse().ToArray(), "S", 83);
                }

                // Termina en 'i' o 'u'
                if(ending[0] == 'i' || ending[0] == 'u')
                {
                    return Normalize(ending.Reverse().ToArray(), "S", 89);
                }
            }

            return Normalize(new string(ending.Skip(1).Reverse().ToArray()), "S", 88);
        }

        static char[] vowels = "aeiou".ToCharArray();
        static char[] consonants = "bcdfghjklmnñpqrstvwxyz".ToCharArray();
        private bool isVowel(char ch)
        {
            char lch = Char.ToLower(ch);

            return vowels.Contains(ch);
        }

        private bool isConsonant(char ch)
        {
            char lch = Char.ToLower(ch);

            return consonants.Contains(ch);
        }

        public bool endsWithVowel(string word)
        {
            return isVowel(word.ToCharArray().Reverse().ToArray()[0]);
        }

        public bool endsWithConsonant(string word)
        {
            return isConsonant(word.ToCharArray().Reverse().ToArray()[0]);
        }

        public char[] reverseArray(string word)
        {
            return word.ToCharArray().Select(cc => Char.ToLower(cc)).Reverse().ToArray();
        }

        public string Normalize(string word, string prefix = null, int line = 0)
        {
            var array = word.ToCharArray();
            return Normalize(array, prefix, line);
        }

        private static string Normalize(char[] array, string prefix = null, int line = 0)
        {
            prefix = prefix == null ? "X" : prefix;
            array[0] = Char.ToUpper(array[0]);


            //return prefix + line.ToString() + "_" + new string(array);

            return new string(array);

        }

        private static string[] SplitCamelCase(string word)
        {
            string sw = "";

            foreach (var ch in word.ToString())
            {
                if (Char.IsUpper(ch))
                {
                    sw += ',';
                }

                sw += ch;
            }

            return sw.Split(new Char[1] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        }

    }
}
